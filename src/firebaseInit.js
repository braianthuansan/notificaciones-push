import firebase from "firebase/app";
import "firebase/messaging";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDPoqDK4s4F0GiAE8Q4DzmofUX2NOZz3gQ",
  authDomain: "cinndet-firebase.firebaseapp.com",
  projectId: "cinndet-firebase",
  storageBucket: "cinndet-firebase.appspot.com",
  messagingSenderId: "223066475833",
  appId: "1:223066475833:web:f8aa74360d940ba38bace8",
  measurementId: "G-NZDP7FWWDJ"

};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

const { REACT_APP_VAPID_KEY } = process.env;
const publicKey = REACT_APP_VAPID_KEY;

export const getToken = async (setTokenFound) => {
  let currentToken = "";

  try {
    currentToken = await messaging.getToken({ vapidKey: publicKey });
    if (currentToken) {
      setTokenFound(true);
    } else {
      setTokenFound(false);
    }
  } catch (error) {
    console.log("An error occurred while retrieving token. ", error);
  }

  return currentToken;
};

export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });
